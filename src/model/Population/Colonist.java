package model.Population;

/**
 * Created by Tomasz.Szczucki on 31.05.2018.
 */
public class Colonist {

    private int farmingSkill;
    private int warriorSkill;
    private int craftsmenSkill;
    private int spiritualSkill;
    private int hunterSkill;
    private int builderSkill;
    private int age;
    private int baseFarmingSkill = 25;
    private int baseWarriorSkill = 25;
    private int baseCraftsmenSkill = 25;
    private int baseSpiritualSkill = 25;
    private int baseHunterSkill = 25;
    private int baseBuilderSkill = 25;
    private int baseStartingAge = 16;


    private String currentOccupation;
    private int maxPossibleSkill = 60;

    public Colonist() {
        age = baseStartingAge;
        farmingSkill = baseFarmingSkill;
        warriorSkill = baseWarriorSkill;
        craftsmenSkill = baseCraftsmenSkill;
        spiritualSkill = baseSpiritualSkill;
        hunterSkill = baseHunterSkill;
        builderSkill = baseBuilderSkill;
        currentOccupation = "none";
    }

    public void train() {
        //weaken
        if (!"warrior".equals(currentOccupation))
            if (warriorSkill > baseWarriorSkill + 2) warriorSkill -= 2;
            else warriorSkill = baseWarriorSkill;
        if (!"farmer".equals(currentOccupation))
            if (farmingSkill > baseFarmingSkill + 2) farmingSkill -= 2;
            else farmingSkill = baseFarmingSkill;
        if (!"priest".equals(currentOccupation))
            if (spiritualSkill > baseSpiritualSkill + 2) spiritualSkill -= 2;
            else spiritualSkill = baseSpiritualSkill;
        if (!"craftsman".equals(currentOccupation))
            if (craftsmenSkill > baseCraftsmenSkill + 2) craftsmenSkill -= 2;
            else craftsmenSkill = baseCraftsmenSkill;
        if (!"hunter".equals(currentOccupation))
            if (hunterSkill > baseHunterSkill + 2) hunterSkill -= 2;
            else hunterSkill = baseHunterSkill;
        if (!"builder".equals(currentOccupation))
            if (builderSkill > baseBuilderSkill + 2) builderSkill -= 2;
            else builderSkill = baseBuilderSkill;
        //train new skill to initial level
        switch (currentOccupation) {
            case "warrior":
                if (warriorSkill < maxPossibleSkill) warriorSkill ++;
                break;
            case "farmer":
                if (farmingSkill < maxPossibleSkill) farmingSkill ++;
                break;
            case "priest":
                if (spiritualSkill < maxPossibleSkill) spiritualSkill ++;
                break;
            case "craftsman":
                if (craftsmenSkill < maxPossibleSkill) craftsmenSkill ++;
                break;
            case "hunter":
                if (hunterSkill < maxPossibleSkill) hunterSkill ++;
                break;
            case "builder":
                if (builderSkill < maxPossibleSkill) builderSkill ++;
                break;

        }

    }

    public void retrain(String newOccupation) {
        //set all other skills to 0
        if (!"warrior".equals(newOccupation))
            this.warriorSkill = baseWarriorSkill;
        if (!"farmer".equals(newOccupation))
            this.farmingSkill = baseFarmingSkill;
        if (!"priest".equals(newOccupation))
            this.spiritualSkill = baseSpiritualSkill;
        if (!"craftsman".equals(newOccupation))
            this.craftsmenSkill = baseCraftsmenSkill;
        if (!"hunter".equals(newOccupation))
            this.hunterSkill = baseHunterSkill;
        if (!"builder".equals(newOccupation))
            this.builderSkill = baseBuilderSkill;
        //train new skill to initial level
        switch (newOccupation) {
            case "warrior":
                warriorSkill = baseWarriorSkill + 10;
                break;
            case "farmer":
                farmingSkill = baseFarmingSkill + 10;
                break;
            case "priest":
                spiritualSkill = baseSpiritualSkill + 10;
                break;
            case "craftsman":
                craftsmenSkill = baseCraftsmenSkill + 10;
                break;
            case "hunter":
                hunterSkill = baseHunterSkill + 10;
                break;
            case "builder":
                builderSkill = baseBuilderSkill + 10;
                break;
        }
    }

    public int getAge() {
        return age;
    }

    public int getFarmingSkill() {
        return farmingSkill;
    }

    public void setFarmingSkill(int farmingSkill) {
        this.farmingSkill = farmingSkill;
    }

    public int getWarriorSkill() {
        return warriorSkill;
    }

    public void setWarriorSkill(int warriorSkill) {
        this.warriorSkill = warriorSkill;
    }

    public int getCraftsmenSkill() {
        return craftsmenSkill;
    }

    public void setCraftsmenSkill(int craftsmenSkill) {
        this.craftsmenSkill = craftsmenSkill;
    }

    public int getSpiritualSkill() {
        return spiritualSkill;
    }

    public void setSpiritualSkill(int spiritualSkill) {
        this.spiritualSkill = spiritualSkill;
    }

    public int getHunterSkill() {
        return hunterSkill;
    }

    public void setHunterSkill(int hunterSkill) {
        this.hunterSkill = hunterSkill;
    }

    public int getBuilderSkill() {
        return builderSkill;
    }

    public void setBuilderSkill(int builderSkill) {
        this.builderSkill = builderSkill;
    }

    public int getBaseFarmingSkill() {
        return baseFarmingSkill;
    }

    public void setBaseFarmingSkill(int baseFarmingSkill) {
        this.baseFarmingSkill = baseFarmingSkill;
    }

    public int getBaseWarriorSkill() {
        return baseWarriorSkill;
    }

    public void setBaseWarriorSkill(int baseWarriorSkill) {
        this.baseWarriorSkill = baseWarriorSkill;
    }

    public int getBaseCraftsmenSkill() {
        return baseCraftsmenSkill;
    }

    public void setBaseCraftsmenSkill(int baseCraftsmenSkill) {
        this.baseCraftsmenSkill = baseCraftsmenSkill;
    }

    public int getBaseSpiritualSkill() {
        return baseSpiritualSkill;
    }

    public void setBaseSpiritualSkill(int baseSpiritualSkill) {
        this.baseSpiritualSkill = baseSpiritualSkill;
    }

    public int getBaseHunterSkill() {
        return baseHunterSkill;
    }

    public void setBaseHunterSkill(int baseHunterSkill) {
        this.baseHunterSkill = baseHunterSkill;
    }

    public int getBaseBuilderSkill() {
        return baseBuilderSkill;
    }

    public void setBaseBuilderSkill(int baseBuilderSkill) {
        this.baseBuilderSkill = baseBuilderSkill;
    }

    public String getCurrentOccupation() {
        return currentOccupation;
    }

    public void setCurrentOccupation(String currentOccupation) {
        this.currentOccupation = currentOccupation;
    }

}
