package model;

/**
 * Created by verit on 26.12.2017.
 */
public enum CurrentMonth {

    JANUARY("Winter", 1),
    FEBRUARY("Winter", 2),
    MARCH("Spring", 3),
    APRIL("Spring", 4),
    MAY("Spring", 5),
    JUNE("Summer", 6),
    JULY("Summer", 7),
    AUGUST("Summer", 8),
    SEPTEMBER("Autumn", 9),
    OCTOBER("Autumn", 10),
    NOVEMBER("Autumn", 11),
    DECEMBER("Winter", 12);


    private String season;
    private int monthNr;

    CurrentMonth(String season, int monthNr) {
        this.season = season;
        this.monthNr = monthNr;
    }

    public String getSeason() {
        return season;
    }
}

