package model;

import javafx.util.Pair;
import model.Cartographer.WorldMap;

import java.util.HashMap;

/**
 * Created by Tomasz.Szczucki on 26.07.2018.
 */
public class Settings {

    private static Settings instance;
    private Integer mapType = WorldMap.HEXAGONAL_POINTY_TOP; //default and only map type for now
    private HashMap<String, Object> defaultSettings;

    public static Settings getInstance() {
        if (instance == null) {
            synchronized (Settings.class) {
                instance = new Settings();
            }
        }
        return instance;
    }

    Settings() {
        //set default settings
        defaultSettings = new HashMap<>();
     //   defaultSettings.put("mapType", WorldMap.HEXAGONAL_POINTY_TOP); // moved to Cartographer
     //   defaultSettings.put("mapSize", new Pair<>(7, 7)); //moved to Cartographer
        defaultSettings.put("difficulty", DifficultySetting.MEDIUM);

    }


    DifficultySetting difficultySetting;

    private Pair<Integer, Integer> worldSize;

    public static void setInstance(Settings instance) {
        Settings.instance = instance;
    }

    public Pair<Integer, Integer> getMapSize() {
        if (worldSize != null) return worldSize;
        return (Pair<Integer, Integer>) defaultSettings.get("mapSize");
    }

    public void setMapSize(Pair<Integer, Integer> worldSize) {
        this.worldSize = worldSize;
    }


    public DifficultySetting getDifficultySetting() {
        if (difficultySetting != null) return difficultySetting;
        return (DifficultySetting) defaultSettings.get("difficulty");
    }

    public void setDifficultySetting(DifficultySetting difficultySetting) {
        this.difficultySetting = difficultySetting;
    }

    public Integer getMapType() {
        if (mapType != null) return mapType;
        return (Integer) defaultSettings.get("mapType");
    }

}
