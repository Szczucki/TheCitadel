package model;

import javafx.scene.paint.Color;
import model.Cartographer.Ifc.TerrainTypeIfc;


/**
 * Created by tomasz.szczucki on 07.11.2018.
 */
//OBSOLETE?
public class Plains implements TerrainTypeIfc {

    String name;
    Color backgroundColor;
    Color strokeColor;
    Double movementPenalty;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backGroundColor) {
        this.backgroundColor = backGroundColor;
    }

    @Override
    public Color getStrokeColor() {
        return strokeColor;
    }

    @Override
    public void setStrokeColor(Color strokeColor) {
        this.strokeColor = strokeColor;
    }

    @Override
    public Double getMass() {
        return null;
    }

    @Override
    public void setMass(Double mass) {

    }

    @Override
    public Double getClustering() {
        return null;
    }

    @Override
    public void setClustering(Double clustering) {

    }
}
