package model.Cartographer;

import javafx.scene.shape.Polygon;
import model.Cartographer.Ifc.TerrainTypeIfc;

/**
 * Created by tomasz.szczucki on 07.11.2018.
 */
public class MapHex {

    protected MapHex() {
        super();
    }

    public MapHex(MapHex mapHex) {

    }

    TerrainTypeIfc terrainType;
    String camp;
    Polygon coordinates;

    public TerrainTypeIfc getTerrainType() {
        return terrainType;
    }

    public void setTerrainType(TerrainTypeIfc terrainType) {
        this.terrainType = terrainType;
    }

    public String getCamp() {
        return camp;
    }

    public void setCamp(String camp) {
        this.camp = camp;
    }

    public Polygon getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Polygon coordinates) {
        this.coordinates = coordinates;
    }
}
