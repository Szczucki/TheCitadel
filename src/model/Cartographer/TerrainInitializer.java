package model.Cartographer;


import javafx.scene.paint.Color;
import javafx.util.Pair;
import model.Cartographer.Ifc.TerrainTypeIfc;

import javax.swing.text.html.HTMLDocument;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by tomasz.szczucki on 07.11.2018.
 */
public class TerrainInitializer {

    HashMap<String, Color> backgroundColorsMap;
    HashMap<String, Color> strokeMap;
    MapPresets mapPresets;


    public HashMap<String, TerrainType> initializeTerrainTypes(MapPresets mapPresets) {
        HashMap<String, TerrainType> terrainTypes = new HashMap<>();
        backgroundColorsMap = new HashMap<>();
        strokeMap = new HashMap<>();
        HashSet<String> terrains = mapPresets.getTerrains();
        for (String terrain : terrains) {
            String[] terrainSplit = terrain.split(";");
            String name = terrainSplit[0];
            TerrainType terrainType = new TerrainType();
            terrainType.setName(name);
            String[] backgroundSplit = terrainSplit[1].split(",");
            String[] strokeSplit = terrainSplit[2].split(",");
            terrainType.setBackgroundColor(Color.rgb(Integer.valueOf(backgroundSplit[0]), Integer.valueOf(backgroundSplit[1]), Integer.valueOf(backgroundSplit[2])));
            terrainType.setStrokeColor(Color.rgb(Integer.valueOf(strokeSplit[0]), Integer.valueOf(strokeSplit[1]), Integer.valueOf(strokeSplit[2])));
            terrainType.setMass(Double.valueOf(terrainSplit[3]));
            terrainType.setClustering(Double.valueOf(terrainSplit[4]));
            terrainTypes.put(name, terrainType);
        }
        return terrainTypes;
    }

    public HashMap<String, Double> createTerrainChanceMap(HashMap<String, TerrainType> terrainsMap) {
        HashMap<String, Double> terrainChanceMap = new HashMap<>();

        Iterator it = terrainsMap.entrySet().iterator();
        Double chanceCeiling = 0.0;
        String terrainName;
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Double terrainMass = ((TerrainType) pair.getValue()).getMass();
            if (terrainMass > 0.0) {
                chanceCeiling += terrainMass;
                terrainName = (String) pair.getKey();
                terrainChanceMap.put(terrainName, chanceCeiling);
            }
        }

        return terrainChanceMap;
    }

    public HashMap<Integer, TerrainTypeIfc> createTerrainChanceMapAlt(HashMap<String, TerrainType> terrainsMap) {
        HashMap<Integer, TerrainTypeIfc> terrainChanceMap = new HashMap<>();

        Iterator it = terrainsMap.entrySet().iterator();

        String terrainName;
        //int prevPutsNumber = 0;

        int i = 0;
        int prevI = 0;

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            TerrainType terrainType = (TerrainType)pair.getValue();
            while (i < terrainType.getMass() + prevI) {
                terrainChanceMap.put(i, terrainType);
                i++;
            }
            prevI = i;
        }

        return terrainChanceMap;
    }

    public Color getBackgroundColor(TerrainTypeIfc terrainType) {
        return backgroundColorsMap.get(terrainType.getName());
    }

    public Color getStroke(TerrainTypeIfc terrainType) {
        return strokeMap.get(terrainType.getName());
    }
}
