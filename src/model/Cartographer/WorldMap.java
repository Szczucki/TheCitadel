package model.Cartographer;

import javafx.geometry.Point2D;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.util.Pair;
import model.Cartographer.Ifc.TerrainTypeIfc;

import java.util.*;

import static java.lang.Math.sqrt;

/**
 * Created by Tomasz.Szczucki on 09.08.2018.
 */
public class WorldMap {

    private Integer mapType;
    private MapHex[][] mapArray;
    private Polygon[][] coordinatesArray;

    private Pair<Integer, Integer> mapSize;
    private MapPresets mapPresets;
    private TerrainInitializer terrainInitializer;
    private HashMap<String, TerrainType> terrainTypes;
    private HashMap<Integer, TerrainTypeIfc> terrainChanceMap;

    public static final Integer HEXAGONAL_POINTY_TOP = 0; //do proper enum!

    public WorldMap(MapPresets mapPresets) {

        this.mapPresets = mapPresets;
        mapSize = new Pair<>(mapPresets.getMapHeight(), mapPresets.getMapWidth());

        mapArray = new MapHex[mapSize.getKey()][mapSize.getValue()];
        this.mapType = mapPresets.getMapType();// == null ? WorldMap.HEXAGONAL_POINTY_TOP : mapType;
        createMapArray(mapArray, mapType);
        terrainInitializer = new TerrainInitializer();
        terrainTypes = terrainInitializer.initializeTerrainTypes(mapPresets);
        terrainChanceMap = terrainInitializer.createTerrainChanceMapAlt(terrainTypes);
        fillTerrainTypes(mapArray, mapPresets);
        markCentralHex();
    }


    private void fillTerrainTypes(MapHex[][] mapArray, MapPresets mapPresets) {
        for (MapHex[] row : mapArray) {
            TerrainType previousTerrainType = null;
            for (MapHex hex : row) {
                if (hex != null) {
                 //  hex.setTerrainType(calculateNextTerrainType(previousTerrainType, terrainTypes));
                    TerrainTypeIfc terrain = terrainChanceMap.get(randomNumber(terrainChanceMap.keySet().size()));
                    hex.setTerrainType(terrain);
                    previousTerrainType = (TerrainType) hex.getTerrainType();
                }
            }
        }
    }

    private int randomNumber(int max) {

        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0; i<100; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        return list.get(0);
    }

    private TerrainType calculateNextTerrainType(TerrainTypeIfc previousTerrainType, Map<String, TerrainType> terrainTypes) {

        Double roll = randomZeroToOne();
        //System.out.print(roll + " ");
        if (roll <= 0.4) System.out.print ("P");
        if (roll > 0.4 && roll <= 0.8) System.out.print("F");
        if (roll > 0.8) System.out.print("W");
        TerrainType nextTerrainType = null;

        if (previousTerrainType == null) {
            Iterator it = terrainChanceMap.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                if (roll < (Double) pair.getValue()) {
                    nextTerrainType = terrainTypes.get(pair.getKey());
                    break;
                }
            }

        } else {
            Double clusteringRoll = randomZeroToOne();
            if (clusteringRoll < previousTerrainType.getClustering()) {
                nextTerrainType = (TerrainType) previousTerrainType;
            } else {
                Iterator it = terrainChanceMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    if (roll < (Double) pair.getValue()) {
                        if (pair.getKey().equals(previousTerrainType.getName())) {
                            nextTerrainType = calculateNextTerrainType(previousTerrainType, terrainTypes);
                            break;
                        } else {
                            nextTerrainType = terrainTypes.get(pair.getKey());
                            break;
                        }
                    }
                }
            }
        }
        return nextTerrainType;
    }


    private void createMapArray(MapHex[][] mapArray, Integer mapType) {

        switch (mapType) {

            case 0:   //do proper enum!
                fillHexShapedMap(mapArray);
                break;

            default:
                break;
        }
    }

    private void fillHexShapedMap(MapHex[][] mapArray) {

        int centralHexY = (mapArray.length - 1) / 2;
        int centralHexX = (mapArray[0].length - 1) / 2;

        for (int y = 0, firstColumn = 0, lastColumn = mapArray[0].length; y < mapArray.length; y++) {
            firstColumn = Math.max(0, centralHexY - y);

            for (int x = firstColumn; x < lastColumn; x++) {
                lastColumn = mapArray[0].length + Math.min(0, centralHexY - y);
                mapArray[y][x] = new MapHex();
            }
        }
    }

    public void drawBoard1(GridPane gridPane) {
//test
        for (int x = 0; x < mapArray.length; x++) {

            for (int y = 0; y < mapArray.length; y++) {
                if (mapArray[x][y] != null) {
                    Label label = new Label("1", new Rectangle(5, 5));
                    gridPane.add(label, x, y);
                } else {
                    Label label = new Label("0", new Rectangle(5, 5));
                    gridPane.add(label, x, y);
                }
            }
            System.out.println();
        }
    }

    public Polygon createHex(int sideLenght, Double startingX, Double startingY, Color fill, Color stroke) {
        int a = sideLenght;
        Point2D startingPoint = new Point2D(startingX, startingY);
        Point2D point1 = startingPoint;
        Point2D point2 = new Point2D(point1.getX() + a * sqrt(3) / 2, point1.getY() + a / 2);
        Point2D point3 = new Point2D(point2.getX(), point2.getY() + a);
        Point2D point4 = new Point2D(point3.getX() - a * sqrt(3) / 2, point3.getY() + a / 2);
        Point2D point5 = new Point2D(point4.getX() - a * sqrt(3) / 2, point4.getY() - a / 2);
        Point2D point6 = new Point2D(point5.getX(), point5.getY() - a);
        Polygon hex = new Polygon(
                point1.getX(), point1.getY(),
                point2.getX(), point2.getY(),
                point3.getX(), point3.getY(),
                point4.getX(), point4.getY(),
                point5.getX(), point5.getY(),
                point6.getX(), point6.getY());
        hex.setFill(fill);
        hex.setStroke(stroke);
        return hex;
    }

    /**
     * @param startingPoint
     * @param sideLength
     * @return array[x][y] of Polygons
     */
    public Polygon[][] createGraphicalMap(Point2D startingPoint, int sideLength) {
        Polygon[][] coordinatesMap = new Polygon[mapSize.getKey()][mapSize.getValue()];
        int centralHexY = (mapArray.length - 1) / 2;
        int centralHexX = (mapArray[0].length - 1) / 2;
        int a = sideLength;

        for (int y = 0; y < mapArray.length; y++) {
            for (int x = 0; x < mapArray[y].length; x++) {
                if (mapArray[y][x] != null) {

                    MapHex mapHex = mapArray[y][x];
                    TerrainType terrainType = (TerrainType) mapHex.getTerrainType();
                    int verticalDistanceFromCenter = y - centralHexY;
                    int horizontalDistanceFromCenter = x - centralHexX;
                    double currentHexY = startingPoint.getY() + verticalDistanceFromCenter * (a * (Math.sqrt(3) / 2) + a / 2);
                    double currentHexX = startingPoint.getX() + horizontalDistanceFromCenter * (a * Math.sqrt(3)) + verticalDistanceFromCenter * (a * (Math.sqrt(3) / 2));
                    Point2D currentHexStartingPoint = new Point2D(currentHexX, currentHexY);
                    Polygon polygon = createHex(sideLength, currentHexStartingPoint.getX(), currentHexStartingPoint.getY(), terrainType.getBackgroundColor(), terrainType.getStrokeColor());
                    coordinatesMap[x][y] = polygon;
                }
            }
        }
        return coordinatesMap;
    }

    private void markCentralHex() {
        int centralHexY = (mapArray.length - 1) / 2;
        int centralHexX = (mapArray[0].length - 1) / 2;
        mapArray[centralHexY][centralHexX].setTerrainType(terrainTypes.get("Special"));
    }

    public MapHex[][] getMapArray() {
        return mapArray;
    }

    public void setMapArray(MapHex[][] mapArray) {
        this.mapArray = mapArray;
    }

    public Double randomZeroToOne() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0; i<100; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        Double randomZeroToOne = (double)list.get(0) / 100;
        return randomZeroToOne;
    }

}
