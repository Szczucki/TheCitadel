package model.Cartographer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * This class loads map.properties file and map all the presets (settings) for the map to be created
 * Created by tomasz.szczucki on 07.11.2018.
 */
public class MapPresets {

    InputStream inputStream;
    /**
     * Between 0 and 1:  1 means 100% land mass, 0 means 100% water
     */
    private int landMass;
    private int mapHeight;
    private int mapWidth;
    private int mapType;
    private String terrain1;
    private String terrain2;
    private String terrain3;
    private String terrain4;
    private String terrain5;
    private String terrain6;
    private String terrain7;
    private String terrain8;
    private String terrain9;
    private String terrain10;
    private HashSet<String> terrains;




    public MapPresets() throws IOException {

        Properties prop = new Properties();
        String propFilePath = "model/Cartographer/map.properties";
        inputStream = getClass().getClassLoader().getResourceAsStream(propFilePath);
        if (inputStream != null) {
            prop.load(inputStream);
        } else {
            throw new FileNotFoundException("property file '" + propFilePath + "' not found in the classpath");
        }

        HashSet<String> terrains = new HashSet<>();
        this.terrains = terrains;

        mapHeight = Integer.valueOf(prop.getProperty("mapHeight"));
        mapWidth = Integer.valueOf(prop.getProperty("mapWidth"));
        landMass = Integer.valueOf(prop.getProperty("landMass"));
        mapType = Integer.valueOf(prop.getProperty("mapType"));

        addTerrainIfNotEmpty("terrain1", prop, terrains);
        addTerrainIfNotEmpty("terrain2", prop, terrains);
        addTerrainIfNotEmpty("terrain3", prop, terrains);
        addTerrainIfNotEmpty("terrain4", prop, terrains);
        addTerrainIfNotEmpty("terrain5", prop, terrains);
        addTerrainIfNotEmpty("terrain6", prop, terrains);
        addTerrainIfNotEmpty("terrain7", prop, terrains);
        addTerrainIfNotEmpty("terrain8", prop, terrains);
        addTerrainIfNotEmpty("terrain9", prop, terrains);
        addTerrainIfNotEmpty("terrain10", prop, terrains);


    }

    public void addTerrainIfNotEmpty(String terrainPropertyName, Properties prop, Collection<String> coll) {
        String terrain = prop.getProperty(terrainPropertyName);
        if (terrain != null && !terrain.isEmpty()) {
            coll.add(terrain);
        }
    }

    public Integer getLandMass() {
        return landMass;
    }

    public void setLandMass(Integer landMass) {
        this.landMass = landMass;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(int mapHeight) {
        this.mapHeight = mapHeight;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(int mapWidth) {
        this.mapWidth = mapWidth;
    }

    public int getMapType() {
        return mapType;
    }

    public void setMapType(int mapType) {
        this.mapType = mapType;
    }


    public String getTerrain1() {
        return terrain1;
    }

    public void setTerrain1(String terrain1) {
        this.terrain1 = terrain1;
    }

    public String getTerrain2() {
        return terrain2;
    }

    public void setTerrain2(String terrain2) {
        this.terrain2 = terrain2;
    }

    public String getTerrain3() {
        return terrain3;
    }

    public void setTerrain3(String terrain3) {
        this.terrain3 = terrain3;
    }

    public String getTerrain4() {
        return terrain4;
    }

    public void setTerrain4(String terrain4) {
        this.terrain4 = terrain4;
    }

    public String getTerrain5() {
        return terrain5;
    }

    public void setTerrain5(String terrain5) {
        this.terrain5 = terrain5;
    }

    public String getTerrain6() {
        return terrain6;
    }

    public void setTerrain6(String terrain6) {
        this.terrain6 = terrain6;
    }

    public String getTerrain7() {
        return terrain7;
    }

    public void setTerrain7(String terrain7) {
        this.terrain7 = terrain7;
    }

    public String getTerrain8() {
        return terrain8;
    }

    public void setTerrain8(String terrain8) {
        this.terrain8 = terrain8;
    }

    public String getTerrain9() {
        return terrain9;
    }

    public void setTerrain9(String terrain9) {
        this.terrain9 = terrain9;
    }

    public String getTerrain10() {
        return terrain10;
    }

    public void setTerrain10(String terrain10) {
        this.terrain10 = terrain10;
    }

    public HashSet<String> getTerrains() {
        return terrains;
    }
}
