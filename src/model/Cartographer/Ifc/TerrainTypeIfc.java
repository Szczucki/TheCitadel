package model.Cartographer.Ifc;

import javafx.scene.paint.Color;

/**
 * Created by tomasz.szczucki on 07.11.2018.
 */
public interface TerrainTypeIfc {

    String getName();
    void setName(String name);

    Color getBackgroundColor();
    void  setBackgroundColor(Color backgroundColor);

    Color getStrokeColor();
    void setStrokeColor (Color strokeColor);

    Double getMass();
    void setMass(Double mass);

    Double getClustering();
    void setClustering(Double clustering);
}
