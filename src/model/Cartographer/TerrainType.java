package model.Cartographer;

import model.Cartographer.Ifc.TerrainTypeIfc;

import javafx.scene.paint.Color;

/**
 * Created by tomasz.szczucki on 07.11.2018.
 */
public class TerrainType implements TerrainTypeIfc {

    String name;
    Color backgroundColor;
    Color strokeColor;
    Double movementPenalty;
    Double mass;
    Double clustering;


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    public void setBackgroundColor(Color backGroundColor) {
        this.backgroundColor = backGroundColor;
    }

    @Override
    public Color getStrokeColor() {
        return strokeColor;
    }

    @Override
    public void setStrokeColor(Color strokeColor) {
        this.strokeColor = strokeColor;
    }

    @Override
    public Double getMass() {
        return mass;
    }

    @Override
    public void setMass(Double mass) {
        this.mass = mass;
    }

    @Override
    public Double getClustering() {
        return clustering;
    }

    @Override
    public void setClustering(Double clustering) {
        this.clustering = clustering;
    }
}
