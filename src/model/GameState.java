package model;

import controllers.Time;
import model.Camps.Camp;
import model.Cartographer.WorldMap;

import java.util.List;

/**
 * Created by verit on 29.12.2017.
 */
public class GameState {


    private GameState() {

    }

    private static GameState gameStateInstance;

    public static GameState getInstance() {
        if (gameStateInstance == null) {
            synchronized (GameState.class) {
                if (gameStateInstance == null) {
                    gameStateInstance = new GameState();
                }
            }
        }
        return gameStateInstance;
    }


    List<Camp> camps;
    Time time = new Time ();
    WorldMap worldMap;

    public void setCamps (List<Camp> camps) {
        this.camps = camps;
    }

    public List<Camp> getCamps() {
        return camps;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public WorldMap getWorldMap() {
        return worldMap;
    }

    public void setWorldMap(WorldMap worldMap) {
        this.worldMap = worldMap;
    }


}
