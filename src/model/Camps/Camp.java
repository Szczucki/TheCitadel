package model.Camps;

import controllers.Observer;
import controllers.Time;
import javafx.beans.property.StringProperty;
import model.*;
import model.Character;
import model.Population.Colonist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by verit on 26.12.2017.
 */
public abstract class Camp implements Observer {

    protected  GameState gameState;
    protected Settings settings;

    protected ArrayList allColonistLists = new ArrayList();
    protected Time time;
    protected StringProperty name;
    protected List<Character> heroes = new ArrayList<>();
    protected List<Building> buildings = new ArrayList<>();
    protected List<Colonist> allColonists;
    protected List<Colonist> freeColonists;
    protected List<Colonist> colonistsAsFarmers;
    protected List<Colonist> colonistsAsHunters;
    protected List<Colonist> colonistsAsBuilders;
    protected List<Colonist> colonistsAsSpiritual;
    protected List<Colonist> colonistsAsCraftsmen;
    protected List<Colonist> colonistsAsWarriors;

    protected int numberOfColonists;
    protected Relations relations;
    protected double populationMorale;
    protected double populationHealth;
    protected int food;
    protected int tradeGoods;


    public abstract void proceedOneMonth();


    public void calculateBasicEconomy() {
        adjustFood();
        adjustTradeGoods();
        adjustPopulation();
    }

    //consume food and use other resources for ongoing maintenance of the camp
    public void maintenance() {
        food -= allColonists.size() * 8;
        if (food < 0) {
            populationHealth -= Math.abs(food)/2;
            populationMorale -= Math.abs(food)/3;
            if (populationHealth < 0) populationHealth = 0;
            if (populationMorale < 0) populationMorale = 0;
            food = 0;
        }
        //morale will slowly rise if there is no shortage of food
        else populationMorale ++;
    }

    Camp(Time time) {
        freeColonists = new ArrayList<>();
        colonistsAsFarmers = new ArrayList<>();
        colonistsAsHunters = new ArrayList<>();
        colonistsAsBuilders = new ArrayList<>();
        colonistsAsSpiritual = new ArrayList<>();
        colonistsAsCraftsmen = new ArrayList<>();
        colonistsAsWarriors = new ArrayList<>();
        allColonists = new ArrayList<>();
        allColonists.addAll(freeColonists);
        allColonists.addAll(colonistsAsBuilders);
        allColonists.addAll(colonistsAsCraftsmen);
        allColonists.addAll(colonistsAsFarmers);
        allColonists.addAll(colonistsAsHunters);
        allColonists.addAll(colonistsAsSpiritual);
        allColonists.addAll(colonistsAsWarriors);
        populationMorale = 100;
        populationHealth = 100;
        allColonistLists.add(freeColonists);
        allColonistLists.add(colonistsAsBuilders);
        allColonistLists.add(colonistsAsCraftsmen);
        allColonistLists.add(colonistsAsFarmers);
        allColonistLists.add(colonistsAsHunters);
        allColonistLists.add(colonistsAsSpiritual);
        allColonistLists.add(colonistsAsWarriors);
        this.gameState = GameState.getInstance();
        this.settings = Settings.getInstance();
        addStartingResources();


        for (int x = 0; x < 10; x++) {
            freeColonists.add(new Colonist());
        }


        numberOfColonists = allColonists.size();
        this.time = time;
        time.attachObserver(this);
    }

    private void addStartingResources() {
        this.food += settings.getDifficultySetting().getFood();

    }

    void adjustFood() {
        int newFoodValue = this.food + calculateProduction("food");
        this.food = newFoodValue;
    }

    void adjustPopulation() {
        allColonists = new ArrayList<>();
        allColonists.addAll(freeColonists);
        allColonists.addAll(colonistsAsBuilders);
        allColonists.addAll(colonistsAsCraftsmen);
        allColonists.addAll(colonistsAsFarmers);
        allColonists.addAll(colonistsAsHunters);
        allColonists.addAll(colonistsAsSpiritual);
        allColonists.addAll(colonistsAsWarriors);
        doBirths();
        doDeaths();
        doDesertions();

    }

    private void doDeaths() {
        double deathChance;
        List <Colonist> deadList = new ArrayList<>();
        for (Colonist colonist : allColonists) {
            deathChance = ((colonist.getAge()-50)/100)/(populationHealth/100);
            if (Math.random() <= deathChance) {
                deadList.add(colonist);
            }
        }
        for (Object list : allColonistLists) {
            ArrayList colonistList = (ArrayList) list;
            colonistList.removeAll(deadList);
        }
    }

    protected void doBirths() {
        double birthChance = 0.01 * ((1 * (populationHealth / 100) + 0.5 * (populationMorale / 100)) * (Math.sqrt(numberOfColonists) * 5));
        if (Math.random() <= birthChance) {
            freeColonists.add(new Colonist());
        }
    }

    protected void doDesertions() {
        double desertionChance;
        List <Colonist> desertersList = new ArrayList<>();
        for (Colonist colonist : allColonists) {
            desertionChance = 1 - (populationMorale/80);
            if (Math.random() <= desertionChance) {
                desertersList.add(colonist);
            }
        }
        for (Object list : allColonistLists) {
            ArrayList colonistList = (ArrayList) list;
            colonistList.removeAll(desertersList);
        }
    }

    void adjustTradeGoods() {
        int newTradeGoodsValue = this.tradeGoods + calculateProduction("tradeGoods");
        this.tradeGoods = newTradeGoodsValue;
    }

    public int calculateProduction(String resourceType) {
        int productionValue = 0;
        switch (resourceType) {

            case "food":
                if (!"winter".equalsIgnoreCase(time.getSeason())) {
                    for (Colonist colonist : colonistsAsFarmers) {
                        productionValue += colonist.getFarmingSkill();
                    }
                    for (Colonist colonist : colonistsAsHunters) {
                        productionValue += colonist.getHunterSkill() * 0.4;
                    }
                }
                break;
            case "tradeGoods":
                for (Colonist colonist : colonistsAsCraftsmen) {
                    productionValue += colonist.getCraftsmenSkill();
                }
                break;
        }
        return productionValue;
    }

    public void addCraftsman() {
        ArrayList<Colonist> craftsmanList = (ArrayList) getColonistsAsCraftsmen();
        ArrayList<Colonist> colnistList = (ArrayList) getFreeColonists();
        if (colnistList.size() >= 1) {
            //find highest skill craftsman among free freeColonists
            int highestSkill = 0;
            int highestSkillColonistIndex = 0;
            int index = -1;
            for (Colonist craftsman : colnistList) {
                index++;
                if (craftsman.getCraftsmenSkill() > highestSkill) {
                    highestSkill = craftsman.getCraftsmenSkill();
                    highestSkillColonistIndex = index;
                }
            }
            colnistList.get(highestSkillColonistIndex).setCurrentOccupation("craftsman");
            craftsmanList.add(colnistList.get(highestSkillColonistIndex));//?
            colnistList.remove(highestSkillColonistIndex);//?
        }
    }

    public void removeCraftsman() {
        ArrayList<Colonist> craftsmanList = (ArrayList) getColonistsAsCraftsmen();
        ArrayList<Colonist> colnistList = (ArrayList) getFreeColonists();
        if (craftsmanList.size() >= 1) {
            //find lowest skill craftsman
            int lowestSkill = 100;
            int lowestSkillColonistIndex = 0;
            int index = -1;
            for (Colonist craftsman : craftsmanList) {
                index++;
                if (craftsman.getCraftsmenSkill() < lowestSkill) {
                    lowestSkill = craftsman.getCraftsmenSkill();
                    lowestSkillColonistIndex = index;
                }
            }
            craftsmanList.get(lowestSkillColonistIndex).setCurrentOccupation("none");
            colnistList.add(craftsmanList.get(lowestSkillColonistIndex));//?
            craftsmanList.remove(lowestSkillColonistIndex);//?
        }
    }

    public void addFarmer() {
        ArrayList<Colonist> farmersList = (ArrayList) getColonistsAsFarmers();
        ArrayList<Colonist> colnistList = (ArrayList) getFreeColonists();
        if (colnistList.size() >= 1) {
            //find highest skill farmer among free freeColonists
            int highestSkill = 0;
            int highestSkillColonistIndex = 0;
            int index = -1;
            for (Colonist farmer : colnistList) {
                index++;
                if (farmer.getFarmingSkill() > highestSkill) {
                    highestSkill = farmer.getFarmingSkill();
                    highestSkillColonistIndex = index;
                }
            }
            colnistList.get(highestSkillColonistIndex).setCurrentOccupation("farmer");
            farmersList.add(colnistList.get(highestSkillColonistIndex));//?
            colnistList.remove(highestSkillColonistIndex);//?
        }
    }

    public void removeFarmer() {
        ArrayList<Colonist> farmerList = (ArrayList) getColonistsAsFarmers();
        ArrayList<Colonist> colnistList = (ArrayList) getFreeColonists();
        if (farmerList.size() >= 1) {
            //find lowest skill farmer
            int lowestSkill = 100;
            int lowestSkillColonistIndex = 0;
            int index = -1;
            for (Colonist farmer : farmerList) {
                index++;
                if (farmer.getFarmingSkill() < lowestSkill) {
                    lowestSkill = farmer.getFarmingSkill();
                    lowestSkillColonistIndex = index;
                }
            }
            farmerList.get(lowestSkillColonistIndex).setCurrentOccupation("none");
            colnistList.add(farmerList.get(lowestSkillColonistIndex));//?
            farmerList.remove(lowestSkillColonistIndex);//?
        }
    }


    public int getFood() {
        return food;
    }

    protected void trainAllColonists(List<Colonist> colonists) {

        for (Colonist colonist : colonists) {
            colonist.train();
        }
    }


    public int getTradeGoods() {
        return tradeGoods;
    }


    public final void setFood(int food) {
        this.food = food;
    }

    public final void setTradeGoods(int tradeGoods) {
        this.tradeGoods = tradeGoods;
    }


    public List<Colonist> getFreeColonists() {
        return freeColonists;
    }

    public void setFreeColonists(List<Colonist> freeColonists) {
        this.freeColonists = freeColonists;
    }

    public List<Colonist> getColonistsAsFarmers() {
        return colonistsAsFarmers;
    }

    public List<Colonist> getColonistsAsHunters() {
        return colonistsAsHunters;
    }

    public List<Colonist> getColonistsAsBuilders() {
        return colonistsAsBuilders;
    }

    public List<Colonist> getColonistsAsSpiritual() {
        return colonistsAsSpiritual;
    }

    public List<Colonist> getColonistsAsCraftsmen() {
        return colonistsAsCraftsmen;
    }

    public List<Colonist> getColonistsAsWarriors() {
        return colonistsAsWarriors;
    }

}
