package model.Camps;

import controllers.Time;
import model.Population.Colonist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by verit on 26.12.2017.
 */
public class PlayerCamp extends Camp {

    public PlayerCamp (Time time) {
        super(time);
    }



    @Override
    public void update() {
    proceedOneMonth();
    }

    @Override
    public void proceedOneMonth() {
       // resolveEvents(new ArrayList<>());
        calculateBasicEconomy();
        for (Colonist colonist : allColonists) {
            colonist.train();
        }
        maintenance();
    }
}
