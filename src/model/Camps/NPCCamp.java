package model.Camps;

import controllers.Time;

/**
 * Created by verit on 26.12.2017.
 */
public abstract class NPCCamp extends Camp {

    @Override
    public void proceedOneMonth() {
        //  resolveEvents(new ArrayList<>());
        calculateBasicEconomy();
    }


    NPCCamp (Time time) {
        super(time);
    }

    @Override
    public void update() {
        proceedOneMonth();
    }


}
