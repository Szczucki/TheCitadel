package model;

/**
 * Created by Tomasz.Szczucki on 26.07.2018.
 */
public enum DifficultySetting {

    EASY(400),
    MEDIUM(250),
    HARD(150);

    private final long food;

    DifficultySetting(long food) {
        this.food = food;
    }

    public long getFood() {
        return food;
    }
}
