package controllers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Cartographer.WorldMap;
import model.GameState;
import model.Settings;


public class TheCitadel extends Application {


    static GameState gameState = GameInitializer.initializeGame(GameState.getInstance(), Settings.getInstance());


    @Override
    public void start(Stage primaryStage) throws Exception {


        //stage and layout
        Stage mainWindow = primaryStage;
        mainWindow.setTitle("The Citadel");
        //GridPane layout = new GridPane();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/MainScreen.fxml"));
        Parent layout = loader.load();
        Scene mainScene = new Scene(layout);
        mainWindow.setScene(mainScene);
        mainWindow.show();
        //set layout

        // layout.getChildren().addAll(nextTurnButton, monthLabel, craftsmenLabel, populationLabel, foodLabel, tradeGoodsLabel, addCreaftsmenButton, removeCreaftsmenButton);
        /*
        layout.setPadding(new Insets(10, 10, 10, 10));
        layout.setVgap(5);
        layout.setHgap(5);
        GridPane.setConstraints(textArea, 1, 0);
        GridPane.setConstraints(nextTurnButton, 1, 2);
        GridPane.setConstraints(monthLabel, 0, 2);
        GridPane.setConstraints(populationLabel, 0, 0);
        GridPane.setConstraints(foodLabel, 0, 1);
        GridPane.setConstraints(tradeGoodsLabel, 1, 1);
        GridPane.setConstraints(addCreaftsmenButton, 2, 0);
        GridPane.setConstraints(removeCreaftsmenButton, 1, 0);
        GridPane.setConstraints(craftsmenLabel, 3, 0);
        */

    }



    public static void main(String[] args) {
        launch(args);
    }

}
