package controllers;

import model.CurrentMonth;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by verit on 26.12.2017.
 */
public class Time implements Observable {

    private CurrentMonth currentMonth = CurrentMonth.JANUARY;
    private List<Observer> observers = new ArrayList<>();

    @Override
    public void attachObserver (Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers () {
        for (Observer observer : observers) {
            observer.update();
        }
    }
    public void proceedGameTime(int turns) {

        for (int i = 0; i < turns; i++) {
            proceedTurn();
        }
    }

    private void proceedTurn() {


        if (currentMonth == currentMonth.JANUARY) {
            currentMonth = currentMonth.FEBRUARY;
        }
        else if (currentMonth == currentMonth.FEBRUARY) {
            currentMonth = currentMonth.MARCH;
        }
        else if (currentMonth == currentMonth.MARCH) {
            currentMonth = currentMonth.APRIL;
        }
        else if (currentMonth == currentMonth.APRIL) {
            currentMonth = currentMonth.MAY;
        }
        else if (currentMonth == currentMonth.MAY) {
            currentMonth = currentMonth.JUNE;
        }
        else if (currentMonth == currentMonth.JUNE) {
            currentMonth = currentMonth.JULY;
        }
        else if (currentMonth == currentMonth.JULY) {
            currentMonth = currentMonth.AUGUST;
        }
        else if (currentMonth == currentMonth.AUGUST) {
            currentMonth = currentMonth.SEPTEMBER;
        }
        else if (currentMonth == currentMonth.SEPTEMBER) {
            currentMonth = currentMonth.OCTOBER;
        }
        else if (currentMonth == currentMonth.OCTOBER) {
            currentMonth = currentMonth.NOVEMBER;
        }
        else if (currentMonth == currentMonth.NOVEMBER) {
            currentMonth = currentMonth.DECEMBER;
        }
        else if (currentMonth == currentMonth.DECEMBER) {
            currentMonth = currentMonth.JANUARY;
        }

        notifyObservers();
    }

    public CurrentMonth getCurrentMonth() {
        return currentMonth;
    }

    public String getSeason() {
        return getCurrentMonth().getSeason();
    }


}
