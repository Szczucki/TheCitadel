package controllers;

/**
 * Created by verit on 29.12.2017.
 */
public interface Observable {

    public void attachObserver (Observer observer);
    public void notifyObservers();
}
