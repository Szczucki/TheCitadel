package controllers;

import model.Camps.Camp;
import model.Camps.PlayerCamp;
import model.Cartographer.MapPresets;
import model.Cartographer.WorldMap;
import model.DifficultySetting;
import model.GameState;
import model.Population.Colonist;
import model.Settings;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by verit on 10.02.2018.
 */
public class GameInitializer {


    public static GameState initializeGame(GameState gameState, Settings settings) {

        settings.setDifficultySetting(DifficultySetting.MEDIUM);
        gameState.setTime(new Time());
        Camp playerCamp = new PlayerCamp(gameState.getTime());
        //playerCamp.setPopulation(1);
        playerCamp.getFreeColonists().add(new Colonist());
  //      Camp orcCamp = new OrcCamp(gameState.getTime());
      //  orcCamp.setPopulation(1);
        List<Camp> camps = new ArrayList<>();
        camps.add(playerCamp);
 //       camps.add(orcCamp);

        gameState.setCamps(camps);





        return gameState;
    }
}
