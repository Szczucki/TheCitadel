package controllers;

/**
 * Created by verit on 29.12.2017.
 */
public interface Observer {

    public abstract void update();
}
