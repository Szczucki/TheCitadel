package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Camps.PlayerCamp;
import model.Cartographer.MapPresets;
import model.Cartographer.WorldMap;

import java.io.IOException;
import java.util.Random;

import static controllers.TheCitadel.gameState;
import static java.lang.Math.sqrt;

/**
 * Created by Tomasz.Szczucki on 29.05.2018.
 */
public class MainController {


    PlayerCamp playerCamp = (PlayerCamp) gameState.getCamps().get(0);
   // WorldMap worldMap = gameState.getWorldMap();

    //Labels
    @FXML
    Label monthLabel;
    @FXML
    Label populationLabel;
    @FXML
    Label foodLabel;
    @FXML
    Label tradeGoodsLabel;
    @FXML
    Label craftsmenLabel;
    @FXML
    Label huntersLabel;
    @FXML
    Label spiritualLabel;
    @FXML
    Label buildersLabel;
    @FXML
    Label farmersLabel;
    @FXML
    Label warriorsLabel;
    @FXML
    Label craftsmanshipSkillLabel;
    @FXML
    Label farmingSkillLabel;


    //Buttons
    @FXML
    Button nextTurnButton;
    @FXML
    Button addCreaftsmenButton;
    @FXML
    Button removeCreaftsmenButton;
    @FXML
    Button addHuntersButton;
    @FXML
    Button removeHuntersButton;
    @FXML
    Button addSpiritualButton;
    @FXML
    Button removeSpiritualButton;
    @FXML
    Button addBuildersButton;
    @FXML
    Button removeBuildersButton;
    @FXML
    Button addFarmersButton;
    @FXML
    Button removeFarmersButton;
    @FXML
    Button addWarriorsButton;
    @FXML
    Button removeWarriorsButton;


    @FXML
    public void initialize() {
        monthLabel.setText("Month: " + gameState.getTime().getCurrentMonth().toString());

        populationLabel.setText("Free freeColonists: " + String.valueOf(playerCamp.getFreeColonists().size()));
        foodLabel.setText("Food: " + String.valueOf(playerCamp.getFood()));
        tradeGoodsLabel.setText("Trade goods: " + String.valueOf(playerCamp.getTradeGoods()));

        craftsmenLabel.setText("Craftsmen: " + String.valueOf(playerCamp.getColonistsAsCraftsmen().size()));
        farmersLabel.setText("Farmers: " + String.valueOf(playerCamp.getColonistsAsFarmers().size()));

        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        BarChart chart1 = new BarChart(xAxis, yAxis);

        XYChart.Series series1 = new XYChart.Series();
        XYChart.Data morale = new XYChart.Data("Morale", 100);
        series1.getData().add(morale);
        chart1.getData().add(series1);

        //buttons
        /*
        nextTurnButton.setOnAction(e -> {
            proceedGameTime();
            monthLabel.setText(gameState.getTime().getCurrentMonth().toString());
            populationLabel.setText("Population: " + String.valueOf(playerCamp.getPopulation()));
            foodLabel.setText("Food: " + String.valueOf(playerCamp.getFood()));
            tradeGoodsLabel.setText("Trade goods: " + String.valueOf(playerCamp.getTradeGoods()));
            craftsmenLabel.setText("Craftsmen: " + String.valueOf(playerCamp.getPopulationAsCraftsmen()));
        });

        addCreaftsmenButton.setOnAction(e -> {
            if (playerCamp.getPopulation() >= 1) {
                playerCamp.setPopulation(playerCamp.getPopulation() - 1);
                populationLabel.setText("Population: " + String.valueOf(playerCamp.getPopulation()));
                playerCamp.setPopulationAsCraftsmen(playerCamp.getPopulationAsCraftsmen() + 1);
                craftsmenLabel.setText("Craftsmen: " + String.valueOf(playerCamp.getPopulationAsCraftsmen()));
            }
        });

        removeCreaftsmenButton.setOnAction(e -> {
            if (playerCamp.getPopulationAsCraftsmen() >= 1) {
                playerCamp.setPopulation(playerCamp.getPopulation() + 1);
                populationLabel.setText("Population: " + String.valueOf(playerCamp.getPopulation()));
                playerCamp.setPopulationAsCraftsmen(playerCamp.getPopulationAsCraftsmen() - 1);
                craftsmenLabel.setText("Craftsmen: " + String.valueOf(playerCamp.getPopulationAsCraftsmen()));
            }
        });
*/

    }

    public void addCreaftsmen() {
        playerCamp.addCraftsman();
        craftsmenLabel.setText("Craftsmen: " + String.valueOf(playerCamp.getColonistsAsCraftsmen().size()));
        populationLabel.setText("Free freeColonists: " + String.valueOf(playerCamp.getFreeColonists().size()));
        craftsmanshipSkillLabel.setText(String.valueOf(playerCamp.calculateProduction("tradeGoods")));
    }

    public void removeCreaftsmen() {
        playerCamp.removeCraftsman();

        craftsmenLabel.setText("Craftsmen: " + String.valueOf(playerCamp.getColonistsAsCraftsmen().size()));
        populationLabel.setText("Free freeColonists: " + String.valueOf(playerCamp.getFreeColonists().size()));
        craftsmanshipSkillLabel.setText(String.valueOf(playerCamp.calculateProduction("tradeGoods")));

    }

    public void addFarmer() {
        playerCamp.addFarmer();

        farmersLabel.setText("Farmer: " + String.valueOf(playerCamp.getColonistsAsFarmers().size()));
        populationLabel.setText("Free freeColonists: " + String.valueOf(playerCamp.getFreeColonists().size()));
        farmingSkillLabel.setText(String.valueOf(playerCamp.calculateProduction("food")));

    }

    public void removeFarmer() {
        playerCamp.removeFarmer();
        farmersLabel.setText("Farmers: " + String.valueOf(playerCamp.getColonistsAsFarmers().size()));
        populationLabel.setText("Free freeColonists: " + String.valueOf(playerCamp.getFreeColonists().size()));
        farmingSkillLabel.setText(String.valueOf(playerCamp.calculateProduction("food")));
    }


    public void nextTurn() {
        gameState.getTime().proceedGameTime(1);
        monthLabel.setText(gameState.getTime().getCurrentMonth().toString());
        populationLabel.setText("Free freeColonists: " + String.valueOf(playerCamp.getFreeColonists().size()));
        farmersLabel.setText("Farmers: " + String.valueOf(playerCamp.getColonistsAsFarmers().size()));
        foodLabel.setText("Food: " + String.valueOf(playerCamp.getFood()));
        tradeGoodsLabel.setText("Trade goods: " + String.valueOf(playerCamp.getTradeGoods()));

        //skill labels
        farmingSkillLabel.setText(String.valueOf(playerCamp.calculateProduction("food")));
        craftsmanshipSkillLabel.setText(String.valueOf(playerCamp.calculateProduction("tradeGoods")));

    }


    public void printMapArray() {


        try {
            Stage mapWindow = new Stage();
            mapWindow.setTitle("Map");
            //GridPane layout = new GridPane();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/Map.fxml"));
            Parent layout = loader.load();
            Scene mapScene = new Scene(layout);
            GridPane gridPane = (GridPane) mapScene.getRoot();
            mapWindow.setScene(mapScene);
            mapWindow.show();
            gameState.getWorldMap().drawBoard1(gridPane);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void generateMap() {

        MapPresets mapPresets = null;
        try {
            mapPresets = new MapPresets();
        } catch (IOException e) {
            e.printStackTrace();
        }
        gameState.setWorldMap(new WorldMap(mapPresets));

    }

    public void printMap() {

//worldMap.get

        Stage mapWindow = new Stage();
        mapWindow.setTitle("Map");
        Group root = new Group();
        Canvas canvas = new Canvas(650, 600);
        //  GraphicsContext gc = canvas.getGraphicsContext2D();

        //  double width = gc.getCanvas().getWidth();
        //  double height = gc.getCanvas().getHeight();

        Random random = new Random(System.currentTimeMillis());

        // gc.setFill(Color.rgb(random.nextInt(255), random.nextInt(255),
        //        random.nextInt(255), 0.9));
        // gc.translate(width / 2, height / 2);
        //  Polygon hex = getHex(50, 50.0, 50.0, Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255)), Color.rgb(100,100,100));

        //   Polygon nextHex = getHex(40, hex.getPoints().get(0)+100, hex.getPoints().get(1)+100, Color.rgb(200,200,200), Color.rgb(100,100,100));

        GridPane coordPane = new GridPane();


        Polygon[][] map = gameState.getWorldMap().createGraphicalMap(new Point2D(250.0, 250.0), 30);
        Polygon hexy;
        int centralHexY = (map.length - 1) / 2;
        int centralHexX = (map[0].length - 1) / 2;
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[x].length; y++) {
                hexy = map[x][y];
                if (hexy != null) {
                    int verticalRelation = y - centralHexY;
                    int horizontalRelation = x - centralHexX;
                    String hexIndex = horizontalRelation + " ; " + verticalRelation;
                    Label hexIndexLabel = new Label(hexIndex);
                    hexIndexLabel.setTranslateY(hexy.getPoints().get(1) + 10);
                    hexIndexLabel.setTranslateX(hexy.getPoints().get(0) - 10);
                    root.getChildren().add(hexy);
                    coordPane.getChildren().add(hexIndexLabel);
                }
            }
        }
        /*
        for (Polygon[] row : map) {
            for (Polygon hex : row) {
                if (hex != null) {
                    root.getChildren().add(hex);
                }
            }
        }*/
        root.getChildren().add(coordPane);
        mapWindow.setScene(new Scene(root));
        mapWindow.show();
    }

    public Polygon getHex(int sideLenght, Double startingX, Double startingY, Color fill, Color stroke) {
        int a = sideLenght;
        Point2D startingPoint = new Point2D(startingX, startingY);
        Point2D point1 = startingPoint;
        Point2D point2 = new Point2D(point1.getX() + a * sqrt(3) / 2, point1.getY() + a / 2);
        Point2D point3 = new Point2D(point2.getX(), point2.getY() + a);
        Point2D point4 = new Point2D(point3.getX() - a * sqrt(3) / 2, point3.getY() + a / 2);
        Point2D point5 = new Point2D(point4.getX() - a * sqrt(3) / 2, point4.getY() - a / 2);
        Point2D point6 = new Point2D(point5.getX(), point5.getY() - a);
        Polygon hex = new Polygon(
                point1.getX(), point1.getY(),
                point2.getX(), point2.getY(),
                point3.getX(), point3.getY(),
                point4.getX(), point4.getY(),
                point5.getX(), point5.getY(),
                point6.getX(), point6.getY());
        hex.setFill(fill);
        hex.setStroke(stroke);
        return hex;
    }
}
